package com.example.producthub.fragments;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.producthub.R;
import com.example.producthub.adapter.CategoryAdapter;
import com.example.producthub.adapter.ProductAdapter;
import com.example.producthub.dialogs.AddCategoryDialog;
import com.example.producthub.screens.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ManageProductFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProductHub";
    RecyclerView rv_product;
    FloatingActionButton fb_add_product;
    ProductAdapter adapter;
    Fragment fragment;

    public ManageProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_product, container, false);

        initComponent(view);

        setAdapter();

        fb_add_product.setOnClickListener(this);

        return view;
    }

    private void initComponent(View view) {
        rv_product = view.findViewById(R.id.rv_product);
        fb_add_product = view.findViewById(R.id.fb_add_product);
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_product.setLayoutManager(linearLayoutManager);
        rv_product.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter = new ProductAdapter(getContext(), MainActivity.lst_product);
        rv_product.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"ManageProductFragement : called");
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if(view == fb_add_product){
            fragment = new AddProductFragment();
            replaceFragment(fragment);
        }
    }

    public void replaceFragment(Fragment someFragment) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.layout_container, someFragment)
                .addToBackStack(null)
                .commit();
    }
}