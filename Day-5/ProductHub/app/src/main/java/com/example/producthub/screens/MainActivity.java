package com.example.producthub.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.example.producthub.R;
import com.example.producthub.fragments.DashboardFragment;
import com.example.producthub.models.Product;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    FrameLayout layout_container;
    Context mContext;

    Fragment fragment;

    public static List<String> lst_category;
    public static List<Product> lst_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        
        initComponent();

        loadFragment(fragment);

    }

    private void initComponent() {
        mContext = this;
        layout_container = findViewById(R.id.layout_container);
        lst_category = new ArrayList<>();
        lst_product = new ArrayList<>();

        fragment = new DashboardFragment();
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.layout_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}