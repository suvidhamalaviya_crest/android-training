package com.example.producthub.fragments;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.producthub.R;
import com.example.producthub.adapter.CategoryAdapter;
import com.example.producthub.dialogs.AddCategoryDialog;
import com.example.producthub.screens.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ManageCategoryFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProductHub";
    RecyclerView rv_category;
    FloatingActionButton fb_add_cat;
    CategoryAdapter adapter;

    public ManageCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_category, container, false);

        initComponent(view);

        setAdapter();

        fb_add_cat.setOnClickListener(this);

        return view;
    }

    private void initComponent(View view) {
        rv_category = view.findViewById(R.id.ll_add);
        fb_add_cat = view.findViewById(R.id.fb_add_cat);
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_category.setLayoutManager(linearLayoutManager);
        rv_category.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter = new CategoryAdapter(getContext(), MainActivity.lst_category);
        rv_category.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if(view == fb_add_cat){
            AddCategoryDialog addCategoryDialog=new AddCategoryDialog(getActivity());
            addCategoryDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            addCategoryDialog.show();
            addCategoryDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    adapter.notifyItemChanged(0);
                }
            });
            Log.e(TAG,"DashboardFragment :: "+ MainActivity.lst_category.size());
        }
    }
}