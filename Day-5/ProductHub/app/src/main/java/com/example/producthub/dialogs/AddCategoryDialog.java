package com.example.producthub.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.producthub.R;
import com.example.producthub.screens.MainActivity;
import com.example.producthub.utils.CommonMethods;

public class AddCategoryDialog extends Dialog implements View.OnClickListener {

    public Activity current_activity;
    public EditText edt_cat_name;
    public Button btn_add_cat;

    public AddCategoryDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.current_activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_category_dialog);

        edt_cat_name = findViewById(R.id.edt_cat_name);
        btn_add_cat = findViewById(R.id.btn_add_cat);

        btn_add_cat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Drawable error_icon = getContext().getResources().getDrawable(R.drawable.ic_error);
        error_icon.setBounds(0, 0, error_icon.getIntrinsicWidth(), error_icon.getIntrinsicHeight());

        if (view == btn_add_cat){
            String cat_name = edt_cat_name.getText().toString().trim();

            if(cat_name.isEmpty()){
                edt_cat_name.setError("Required",error_icon);
                return;
            }

            MainActivity.lst_category.add(0,cat_name);
            CommonMethods.showToast(getContext(),current_activity.getString(R.string.add_success));
        }
        dismiss();
    }
}
