package com.example.producthub.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.producthub.R;
import com.example.producthub.models.Product;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    List<Product> lst_Product;
    Context context;

    public ProductAdapter(Context context, List<Product> lst_Product) {
        this.lst_Product = lst_Product;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_pro_name.setText(lst_Product.get(position).getProName());
        holder.tv_price.setText("$"+lst_Product.get(position).getProPrice());
        holder.tv_cat.setText(lst_Product.get(position).getCategory());
        holder.tv_sh.setText((lst_Product.get(position).getIsSecondHand().equals("Yes"))?"Second Hand":"Fresh");
        holder.tv_avail.setText((lst_Product.get(position).getIsAvail().equals("Yes"))?"Available":"Out Of Stock");
        holder.tv_type.setText(lst_Product.get(position).getType());
    }

    @Override
    public int getItemCount() {
        return lst_Product.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_pro_name,tv_price,tv_cat,tv_type,tv_sh,tv_avail;
        CardView cv_product;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_pro_name = itemView.findViewById(R.id.tv_pro_name);
            tv_price = itemView.findViewById(R.id.tv_pro_price);
            tv_avail = itemView.findViewById(R.id.tv_avail);
            tv_cat = itemView.findViewById(R.id.tv_pro_cat);
            tv_type = itemView.findViewById(R.id.tv_type);
            tv_sh = itemView.findViewById(R.id.tv_sh);
            cv_product = itemView.findViewById(R.id.cv_product);
        }
    }
}
