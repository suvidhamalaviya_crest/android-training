package com.example.producthub.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.producthub.R;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<String> lst_Categorys;
    Context context;

    public CategoryAdapter(Context context, List<String> lst_Category) {
        this.lst_Categorys = lst_Category;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.list_tile.setText(lst_Categorys.get(position));
    }

    @Override
    public int getItemCount() {
        return lst_Categorys.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView list_tile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            list_tile = itemView.findViewById(R.id.list_tile);
        }
    }
}
