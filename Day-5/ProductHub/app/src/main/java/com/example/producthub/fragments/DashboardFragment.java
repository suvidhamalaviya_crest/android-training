package com.example.producthub.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.producthub.R;
import com.example.producthub.dialogs.AddCategoryDialog;
import com.example.producthub.screens.MainActivity;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProductHub";
    CardView cv_manage_category,cv_manage_product;
    Fragment fragment;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        initComponent(view);

        cv_manage_category.setOnClickListener(this);
        cv_manage_product.setOnClickListener(this);

        return view;
    }


    private void initComponent(View view) {
        cv_manage_product = view.findViewById(R.id.cv_manage_product);
        cv_manage_category = view.findViewById(R.id.cv_manage_category);
    }

    @Override
    public void onClick(View view) {
        if(view == cv_manage_category){
            fragment = new ManageCategoryFragment();
            replaceFragment(fragment);
        }
        else if(view == cv_manage_product){
            fragment = new ManageProductFragment();
            replaceFragment(fragment);
        }
    }

    public void replaceFragment(Fragment someFragment) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.layout_container, someFragment)
                .addToBackStack(null)
                .commit();
    }
}