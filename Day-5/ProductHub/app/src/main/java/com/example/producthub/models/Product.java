package com.example.producthub.models;

public class Product {
    String proName,proPrice,type,isSecondHand,category,isAvail;

    public Product(String proName, String proPrice, String type, String isSecondHand, String category, String isAvail) {
        this.proName = proName;
        this.proPrice = proPrice;
        this.type = type;
        this.isSecondHand = isSecondHand;
        this.category = category;
        this.isAvail = isAvail;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProPrice() {
        return proPrice;
    }

    public void setProPrice(String proPrice) {
        this.proPrice = proPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsSecondHand() {
        return isSecondHand;
    }

    public void setIsSecondHand(String isSecondHand) {
        this.isSecondHand = isSecondHand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIsAvail() {
        return isAvail;
    }

    public void setIsAvail(String isAvail) {
        this.isAvail = isAvail;
    }
}
