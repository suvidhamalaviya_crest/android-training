package com.example.producthub;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.producthub.screens.MainActivity;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_TIME_OUT = 3000;
    Context mContext;

    ImageView iv_logo;
    TextView tv_title;

    Animation top_animation,middle_animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        initComponent();

        setAnimation();

        goToNext();
    }

    private void initComponent() {
        mContext = this;
        iv_logo = findViewById(R.id.iv_logo);
        tv_title = findViewById(R.id.tv_title);

        top_animation = AnimationUtils.loadAnimation(mContext,R.anim.top_animation);
        middle_animation = AnimationUtils.loadAnimation(mContext,R.anim.middle_animation);
    }

    private void setAnimation() {
        iv_logo.setAnimation(top_animation);
        tv_title.setAnimation(middle_animation);
    }

    private void goToNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
}