package com.example.producthub.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;

import com.example.producthub.R;
import com.example.producthub.models.Product;
import com.example.producthub.screens.MainActivity;
import com.example.producthub.utils.CommonMethods;

import java.util.Collections;

public class AddProductFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProductHub";
    Button btn_add_product;
    Spinner sp_category;
    EditText edt_pro_name,edt_price;
    RadioButton rb_hard,rb_soft;
    CheckBox chk_avail;
    Switch sw_sh;
    ArrayAdapter adapter;

    public AddProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_product, container, false);

        initComponent(view);

        btn_add_product.setOnClickListener(this);

        return view;
    }

    private void initComponent(View view) {
        btn_add_product = view.findViewById(R.id.btn_add_product);
        sp_category = view.findViewById(R.id.sp_category);
        edt_pro_name = view.findViewById(R.id.edt_pro_name);
        edt_price = view.findViewById(R.id.edt_price);
        rb_hard = view.findViewById(R.id.rb_hard);
        rb_soft = view.findViewById(R.id.rb_soft);
        chk_avail = view.findViewById(R.id.chk_avail);
        sw_sh = view.findViewById(R.id.sw_sh);

        adapter = new ArrayAdapter(getContext(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,MainActivity.lst_category);

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        sp_category.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        if(view == btn_add_product){
            String pro_name = edt_pro_name.getText().toString().trim();
            String price = edt_price.getText().toString().trim();
            String category = sp_category.getSelectedItem().toString();
            String type = (rb_hard.isChecked())?"Hardware":"Software";
            String avail = (chk_avail.isChecked())?"Yes":"No";
            String sh = (sw_sh.isChecked())?"Yes":"No";

            if(pro_name.isEmpty()){
                CommonMethods.showToast(getContext(),getString(R.string.pro_name_error));
                return;
            }
            else if(price.isEmpty()){
                CommonMethods.showToast(getContext(),getString(R.string.price_error));
                return;
            }
            else{
                Product product = new Product(pro_name,price,type,sh,category,avail);
                MainActivity.lst_product.add(product);
                CommonMethods.showToast(getContext(),getString(R.string.product_added));
                Log.e(TAG,"AddProductFragment"+MainActivity.lst_product.size());
                getActivity().onBackPressed();
            }
        }
    }
}