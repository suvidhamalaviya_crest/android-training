package com.example.flashonu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flashonu.Screens.MainActivity;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv_logo;
    TextView tv_title;
    Button btn_next;

    Animation blink,slide;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initComponent();

        setAnimation();
    }

    private void initComponent() {
        iv_logo = findViewById(R.id.iv_logo);
        tv_title = findViewById(R.id.tv_title);
        btn_next = findViewById(R.id.btn_next);

        blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        slide = AnimationUtils.loadAnimation(this,R.anim.bottom);

        btn_next.setOnClickListener(this);

        mContext = this;
    }

    private void setAnimation() {
        iv_logo.setAnimation(blink);
        tv_title.setAnimation(slide);
        btn_next.setAnimation(slide);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        finish();
    }
}