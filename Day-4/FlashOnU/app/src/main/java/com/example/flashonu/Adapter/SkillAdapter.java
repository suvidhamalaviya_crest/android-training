package com.example.flashonu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flashonu.R;
import com.example.flashonu.models.Skill;

import java.util.List;

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.ViewHolder> {

    List<Skill> lst_skills;
    Context context;

    public SkillAdapter(Context context, List<Skill> lst_skill) {
        this.lst_skills = lst_skill;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name.setText(lst_skills.get(position).getSkillName());
        holder.ratingBar.setText(lst_skills.get(position).getRating());
    }

    @Override
    public int getItemCount() {
        return lst_skills.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name,ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_skill_name);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }
}
