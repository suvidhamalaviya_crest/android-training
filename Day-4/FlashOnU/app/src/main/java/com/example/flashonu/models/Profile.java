package com.example.flashonu.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Profile implements Serializable {

    private String name,technology,birthdate,logTime,gender,isHandicape,tcAccepted;
    private List<Skill> lst_skill;

    public Profile(String name, String technology, String birthdate, String logTime, String gender, String isHandicape, String tcAccepted) {
        this.name = name;
        this.technology = technology;
        this.birthdate = birthdate;
        this.logTime = logTime;
        this.gender = gender;
        this.isHandicape = isHandicape;
        this.tcAccepted = tcAccepted;
        lst_skill = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIsHandicape() {
        return isHandicape;
    }

    public void setIsHandicape(String isHandicape) {
        this.isHandicape = isHandicape;
    }

    public String getTcAccepted() {
        return tcAccepted;
    }

    public void setTcAccepted(String tcAccepted) {
        this.tcAccepted = tcAccepted;
    }

    public List<Skill> getLst_skill() {
        return lst_skill;
    }

    public void setLst_skill(List<Skill> lst_skill) {
        this.lst_skill = lst_skill;
    }
}
