package com.example.flashonu.Screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.example.flashonu.R;
import com.example.flashonu.Utils.CommonMethods;
import com.example.flashonu.models.Profile;
import com.example.flashonu.models.Skill;

import java.util.List;

public class AddSkillsActivity extends AppCompatActivity implements View.OnClickListener {

    Profile profile;
    List<Skill> lst_skills;

    EditText edt_skill_name;
    RatingBar ratingBar;
    Button btn_add_skill;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryVariant));

        setContentView(R.layout.activity_add_skills);

        initComponent();
    }

    private void initComponent() {
        mContext = this;
        profile = (Profile) getIntent().getSerializableExtra("Profile");
        lst_skills = profile.getLst_skill();

        edt_skill_name = findViewById(R.id.edt_skill_name);
        ratingBar = findViewById(R.id.ratingBar);
        btn_add_skill = findViewById(R.id.btn_add_skill);

        btn_add_skill.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String skill_name = edt_skill_name.getText().toString().trim();
        String rating = String.valueOf(ratingBar.getRating()).trim();

        if(skill_name.isEmpty()){
            CommonMethods.showToast(mContext,getString(R.string.skill_name_error));
            return;
        }
        else{
            Skill skill = new Skill(skill_name,rating);
            lst_skills.add(0,skill);
            profile.setLst_skill(lst_skills);

            Intent returnIntent = new Intent();
            returnIntent.putExtra("Profile",profile);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }
}