package com.example.flashonu.Screens;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.flashonu.Adapter.SkillAdapter;
import com.example.flashonu.R;
import com.example.flashonu.Utils.CommonMethods;
import com.example.flashonu.models.Profile;
import com.example.flashonu.models.Skill;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class ShowProfileActivity extends AppCompatActivity implements View.OnClickListener {

    static Profile profile;
    TextView tv_name, tv_tech, tv_bdate, tv_log_time, tv_gender, tv_ph_hand, tv_tc;
    RecyclerView rv_skills;
    Button btn_add_skills, btn_share_ss;
    Context mContext;

    SkillAdapter adapter;
    List<Skill> lst_skill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryVariant));

        setContentView(R.layout.activity_show_profile);

        initComponent();
    }

    private void initComponent() {
        mContext = this;
        profile = (Profile) getIntent().getSerializableExtra("Profile");
        lst_skill = profile.getLst_skill();

        tv_name = findViewById(R.id.tv_name);
        tv_tech = findViewById(R.id.tv_tech);
        tv_bdate = findViewById(R.id.tv_bdate);
        tv_log_time = findViewById(R.id.tv_log_time);
        tv_gender = findViewById(R.id.tv_gender);
        tv_ph_hand = findViewById(R.id.tv_ph_hand);
        tv_tc = findViewById(R.id.tv_tc);
        rv_skills = findViewById(R.id.lv_skills);
        btn_add_skills = findViewById(R.id.btn_add_skills);
        btn_share_ss = findViewById(R.id.btn_share_ss);

        //lst_skill.add(new Skill("sad", "1.0"));

        tv_name.setText(profile.getName());
        tv_tech.setText(profile.getTechnology());
        tv_bdate.setText(profile.getBirthdate());
        tv_log_time.setText(profile.getLogTime());
        tv_gender.setText(profile.getGender());
        tv_ph_hand.setText(profile.getIsHandicape());
        tv_tc.setText(profile.getTcAccepted());

        //if(lst_skill!=null && lst_skill.size()>0)
        setAdapter();

        btn_add_skills.setOnClickListener(this);
        btn_share_ss.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == btn_add_skills) {
            Intent intent = new Intent(mContext, AddSkillsActivity.class);
            intent.putExtra("Profile", profile);
            resultLauncher.launch(intent);
        } else if (view == btn_share_ss) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);

            Bitmap bitmap = CommonMethods.screenShot(rootView);
            CommonMethods.share(mContext,bitmap);
        }
    }

    ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK) {
                Intent intent = result.getData();
                if (!lst_skill.isEmpty())
                    lst_skill.clear();

                profile = (Profile) intent.getSerializableExtra("Profile");
                lst_skill = profile.getLst_skill();

                rv_skills.getRecycledViewPool().clear();
                setAdapter();
                //adapter.notifyDataSetChanged();
                //adapter.notifyItemInserted(0);
                Log.e("Size", String.valueOf(lst_skill.size()));
                //Log.e("Size",lst_skill.get(0).getSkillName().toString());
            }
        }
    });

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        rv_skills.setLayoutManager(linearLayoutManager);
        rv_skills.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new SkillAdapter(this, lst_skill);
        rv_skills.setAdapter(adapter);
    }


}