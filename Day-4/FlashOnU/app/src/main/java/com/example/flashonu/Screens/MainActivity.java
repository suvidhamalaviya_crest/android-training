package com.example.flashonu.Screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.flashonu.R;
import com.example.flashonu.Utils.CommonMethods;
import com.example.flashonu.models.Profile;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    Context mContext;
    TextView tv_title;
    EditText edt_name,edt_bdate,edt_log_time;
    AutoCompleteTextView edt_technology;
    RadioGroup rg_gender;
    RadioButton rb_male,rb_female,rb_other;
    Switch sw_physically_handicape;
    CheckBox chk_tc;
    Button btn_next;

    int day,month,year,hour,minute;
    List<String> lst_technologies;
    ArrayAdapter<String> ad_tech;
    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryVariant));
        setContentView(R.layout.activity_main);

        initComponent();
    }

    private void initComponent() {
        mContext = this;
        tv_title = findViewById(R.id.tv_title);
        edt_name = findViewById(R.id.edt_name);
        edt_technology = findViewById(R.id.edt_technology);
        edt_bdate = findViewById(R.id.edt_bdate);
        edt_log_time = findViewById(R.id.edt_log_time);
        rg_gender = findViewById(R.id.rg_gender);
        rb_male = findViewById(R.id.rb_male);
        rb_female = findViewById(R.id.rb_female);
        rb_other = findViewById(R.id.rb_other);
        sw_physically_handicape = findViewById(R.id.sw_physically_handicape);
        chk_tc = findViewById(R.id.chk_tc);
        btn_next = findViewById(R.id.btn_next);

        btn_next.setOnClickListener(this);
        edt_bdate.setOnClickListener(this);
        edt_log_time.setOnClickListener(this);

        lst_technologies = new ArrayList<String>();
        lst_technologies.add("Android");
        lst_technologies.add("Java");
        lst_technologies.add("Kotlin");
        lst_technologies.add("Flutter");
        lst_technologies.add("React");
        lst_technologies.add("PHP");
        lst_technologies.add("Wordpress");
        lst_technologies.add(".NET");
        lst_technologies.add("C");
        lst_technologies.add("C++");
        lst_technologies.add("IOS");
        lst_technologies.add("ASP.NET");
        lst_technologies.add("Node JS");

        ad_tech = new ArrayAdapter<>(mContext, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,lst_technologies);
        edt_technology.setThreshold(1);

        edt_technology.setAdapter(ad_tech);
    }

    @Override
    public void onClick(View view) {
        if(view == btn_next){
            String name = edt_name.getText().toString().trim();
            String tech = edt_technology.getText().toString().trim();
            String bDate = edt_bdate.getText().toString().trim();
            String logTime = edt_log_time.getText().toString().trim();
            String isHandicape = sw_physically_handicape.isChecked() ? "Yes" : "No";
            String tcAccepted = chk_tc.isChecked() ? "Yes" : "No";
            String gender = "";
            if (rb_male.isChecked())
                gender = rb_male.getText().toString();
            else if(rb_female.isChecked())
                gender = rb_female.getText().toString();
            else
                gender = rb_other.getText().toString();

            if(name.isEmpty()){
                CommonMethods.showToast(mContext,getString(R.string.name_error));
                return;
            }
            else if(tech.isEmpty()){
                CommonMethods.showToast(mContext,getString(R.string.tech_error));
                return;
            }
            else if(bDate.isEmpty()){
                CommonMethods.showToast(mContext,getString(R.string.bdate_error));
                return;
            }
            else if(logTime.isEmpty()){
                CommonMethods.showToast(mContext,getString(R.string.logtime_error));
                return;
            }
            else if(tcAccepted.equals("No")){
                CommonMethods.showToast(mContext,getString(R.string.tc_error));
                return;
            }
            else {
                profile = new Profile(name,tech,bDate,logTime,gender,isHandicape,tcAccepted);
                Log.e("Profile", String.valueOf(profile));

                Intent intent = new Intent(mContext,ShowProfileActivity.class);
                intent.putExtra("Profile",profile);
                startActivity(intent);
            }
        }
        else if(view == edt_bdate){
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DATE);

            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,this,year,month,day);
            datePickerDialog.show();
        }
        else if(view == edt_log_time){
            Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR);
            minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,this,hour,minute, DateFormat.is24HourFormat(this));
            timePickerDialog.show();
        }

        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        edt_bdate.setText(String.valueOf(d)+"/"+String.valueOf(m)+"/"+String.valueOf(y));
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int h, int m) {
        hour = h;
        minute = m;
        edt_log_time.setText(String.valueOf(h)+"h "+String.valueOf(m)+"m");
    }
}