
package com.example.passanger.models;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unused")
public class Passenger implements Serializable {

    private List<Datum> data;
    private Long totalPages;
    private Long totalPassengers;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public Long getTotalPassengers() {
        return totalPassengers;
    }

    public void setTotalPassengers(Long totalPassengers) {
        this.totalPassengers = totalPassengers;
    }

    @Override
    public String toString() {
        return "Passanger{" +
                "data=" + data +
                ", totalPages=" + totalPages +
                ", totalPassengers=" + totalPassengers +
                '}';
    }
}
