package com.example.passanger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.passanger.adapter.PassangerAdapter;
import com.example.passanger.api.MyRetrofitClient;
import com.example.passanger.api.MyRetrofitService;
import com.example.passanger.models.Datum;
import com.example.passanger.models.Passenger;
import com.example.passanger.screens.AddPassangerActivity;
import com.example.passanger.screens.DetailViewActivity;
import com.example.passanger.utils.CommonMethods;
import com.example.passanger.utils.PassangerOnClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements PassangerOnClickListener, View.OnClickListener {

    RecyclerView rv_passenger;
    FloatingActionButton fb_add_pass;
    TextView tv_title;
    Context mContext;
    Animation top_animation;

    RecyclerView.LayoutManager layoutManager;

    List<Datum> lst_passanger;
    PassangerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        initComponent();

        setAnimation();

        selectPassanger();
    }

    @Override
    protected void onResume() {
        super.onResume();
        lst_passanger.clear();
        selectPassanger();
    }

    private void initComponent() {
        mContext = this;
        tv_title = findViewById(R.id.tv_title);
        fb_add_pass = findViewById(R.id.fb_add_pass);
        rv_passenger = findViewById(R.id.rv_passenger);

        lst_passanger = new ArrayList<>();

        top_animation = AnimationUtils.loadAnimation(mContext,R.anim.top_animation);

        fb_add_pass.setOnClickListener(this);

        layoutManager = new GridLayoutManager(mContext, 2);
        rv_passenger.setLayoutManager(layoutManager);
    }

    private void setAnimation() {
        tv_title.setAnimation(top_animation);
    }

    private void selectPassanger(){

        MyRetrofitService myRetrofitService = MyRetrofitClient.getClient();
        Call<Passenger> call = myRetrofitService.selectPassanger();

        call.enqueue(new Callback<Passenger>() {
            @Override
            public void onResponse(Call<Passenger> call, Response<Passenger> response) {
                Passenger passenger = response.body();
                lst_passanger = passenger.getData();
                adapter = new PassangerAdapter(mContext,lst_passanger,MainActivity.this);
                rv_passenger.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Passenger> call, Throwable t) {
                CommonMethods.showToast(mContext,"Can't Get Data",R.drawable.ic_error);
            }
        });

    }

    @Override
    public void onPassangerClick(Datum passanger, String myAction) {
        if(myAction.equals("View")){
            Intent intent = new Intent(mContext, DetailViewActivity.class);
            intent.putExtra("passenger",passanger);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == fb_add_pass){
            Intent intent = new Intent(mContext, AddPassangerActivity.class);
            startActivity(intent);
        }
    }
}