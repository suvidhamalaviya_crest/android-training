
package com.example.passanger.models;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Airline implements Serializable {

    private String country;
    private String established;
    private String headQuaters;
    private String id;
    private String logo;
    private String name;
    private String slogan;
    private String website;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEstablished() {
        return established;
    }

    public void setEstablished(String established) {
        this.established = established;
    }

    public String getHeadQuaters() {
        return headQuaters;
    }

    public void setHeadQuaters(String headQuaters) {
        this.headQuaters = headQuaters;
    }

    public String getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = String.valueOf(id);
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}
