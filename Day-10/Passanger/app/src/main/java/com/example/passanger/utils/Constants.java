package com.example.passanger.utils;

public interface Constants {
    String BASE_URL="https://api.instantwebtools.net/v1/";

    String SELECT_PASSENGER_DATA ="passenger?page=0&size=100";
    String SELECT_AIRLINE_DATA ="airlines?page=0&size=20";
    String ADD_PASSENGER_DATA ="passenger";

    String NAME = "name";
    String TRIPS = "trips";
    String AIRLINES = "airline";

}
