package com.example.passanger.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.passanger.MainActivity;
import com.example.passanger.R;
import com.example.passanger.adapter.PassangerAdapter;
import com.example.passanger.api.MyRetrofitClient;
import com.example.passanger.api.MyRetrofitService;
import com.example.passanger.models.Airline;
import com.example.passanger.models.Passenger;
import com.example.passanger.utils.CommonMethods;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "Passenger";
    ImageView iv_logo;
    TextView tv_title,tv_sub_title;
    Animation top_animation,middle_animation,bottom_animation;
    Context mContext;

    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        initComponent();

        setAnimation();

        goNext();
    }

    private void initComponent() {
        mContext = this;
        iv_logo = findViewById(R.id.iv_logo);
        tv_title = findViewById(R.id.tv_title);
        tv_sub_title = findViewById(R.id.tv_sub_title);

        top_animation = AnimationUtils.loadAnimation(mContext,R.anim.top_animation);
        middle_animation = AnimationUtils.loadAnimation(mContext,R.anim.middle_animation);
        bottom_animation = AnimationUtils.loadAnimation(mContext,R.anim.botton_animation);
    }

    private void setAnimation(){
        iv_logo.setAnimation(top_animation);
        tv_title.setAnimation(middle_animation);
        tv_sub_title.setAnimation(bottom_animation);
    }

    private void goNext(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_TIME_OUT);
    }

}