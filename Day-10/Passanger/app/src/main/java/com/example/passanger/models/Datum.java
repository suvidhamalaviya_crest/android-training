
package com.example.passanger.models;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unused")
public class Datum implements Serializable {

    private Long _V;
    private String _id;
    private List<Airline> airline;
    private String name;
    private Long trips;

    public Long get_V() {
        return _V;
    }

    public void set_V(Long _V) {
        this._V = _V;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Airline> getAirline() {
        return airline;
    }

    public void setAirline(List<Airline> airline) {
        this.airline = airline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTrips() {
        return trips;
    }

    public void setTrips(Long trips) {
        this.trips = trips;
    }

}
