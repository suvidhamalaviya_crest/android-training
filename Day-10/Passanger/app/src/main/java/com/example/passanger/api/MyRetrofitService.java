package com.example.passanger.api;

import com.example.passanger.models.Airline;
import com.example.passanger.models.Passenger;
import com.example.passanger.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MyRetrofitService {

    @GET(Constants.SELECT_PASSENGER_DATA)
    Call<Passenger> selectPassanger();

    @GET(Constants.SELECT_AIRLINE_DATA)
    Call<List<Airline>> selectAirLines();

    @FormUrlEncoded
    @POST(Constants.ADD_PASSENGER_DATA)
    Call<String> insertPassenger(@Field(Constants.NAME) String name,
                            @Field(Constants.TRIPS) String trips,
                            @Field(Constants.AIRLINES) List<Airline> airlines);

}
