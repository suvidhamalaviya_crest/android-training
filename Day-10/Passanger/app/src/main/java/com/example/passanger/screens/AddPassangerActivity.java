package com.example.passanger.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.passanger.R;
import com.example.passanger.adapter.AirLinesAdapter;
import com.example.passanger.api.MyRetrofitClient;
import com.example.passanger.api.MyRetrofitService;
import com.example.passanger.models.Airline;
import com.example.passanger.utils.CommonMethods;
import com.example.passanger.utils.OnFetchingDone;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPassangerActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    TextView tv_title;
    EditText edt_name,edt_trip;
    Spinner sp_airline;
    ImageButton btn_add_airline;
    RecyclerView rv_airline;
    Button btn_add_passenger;

    ArrayAdapter airline_adapter_name;
    AirLinesAdapter airline_adapter_list;

    List<Airline> lst_airline;

    Animation top_animation;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_passanger);

        initComponent();

        setAnimation();
    }

    private void initComponent() {
        mContext = this;
        tv_title = findViewById(R.id.tv_title);
        edt_name = findViewById(R.id.edt_name);
        edt_trip = findViewById(R.id.edt_trip);
        sp_airline = findViewById(R.id.sp_airline);
        btn_add_airline = findViewById(R.id.btn_add_airline);
        rv_airline = findViewById(R.id.rv_airline);
        btn_add_passenger = findViewById(R.id.btn_add_passenger);

        btn_add_passenger.setOnClickListener(this);
        btn_add_airline.setOnClickListener(this);
        progressDialog = new ProgressDialog(mContext);

        progressDialog.setMessage("Please wait...Airlines Are Fetching");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        lst_airline = new ArrayList<>();

        top_animation = AnimationUtils.loadAnimation(mContext, R.anim.top_animation);

        airline_adapter_name = new ArrayAdapter(mContext, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, CommonMethods.lst_airlines_name);
        airline_adapter_name.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
        sp_airline.setAdapter(airline_adapter_name);

        rv_airline.setLayoutManager(new GridLayoutManager(this, 2));
        airline_adapter_list = new AirLinesAdapter(this, lst_airline);
        rv_airline.setAdapter(airline_adapter_list);

        AsyncFetchAirlines asyncFetchAirlines = new AsyncFetchAirlines(mContext,progressDialog);
        asyncFetchAirlines.execute();

    }

    private void setAnimation() {
        tv_title.setAnimation(top_animation);
    }

    @Override
    public void onClick(View view) {
        if(view == btn_add_airline){
            lst_airline.add(CommonMethods.lst_airlines.get(sp_airline.getSelectedItemPosition()));
            airline_adapter_list.notifyDataSetChanged();
        }else if (view == btn_add_passenger){
            String name = edt_name.getText().toString().trim();
            String trip = edt_name.getText().toString().trim();

            if (name.isEmpty()){
                CommonMethods.showToast(mContext,"Please Provide Name",R.drawable.ic_error);
                return;
            }
            else if (trip.isEmpty()){
                CommonMethods.showToast(mContext,"Please Provide Trip Detail",R.drawable.ic_error);
                return;
            }
            else{
                addNewData(name,trip);
                finish();
            }
        }
    }

    private void addNewData(String name, String trip) {
        MyRetrofitService myRetrofitService = MyRetrofitClient.getClient();
        Call<String> call = myRetrofitService.insertPassenger(name,trip,lst_airline);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String msg = response.body();
                CommonMethods.showToast(mContext,"Added Successfully",R.drawable.ic_done);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonMethods.showToast(mContext,"Can't Add Data",R.drawable.ic_error);
            }
        });
    }

    public class AsyncFetchAirlines extends AsyncTask<String,Void, Void> {

        private static final String TAG = "Passenger";
        Context context;
        ProgressDialog progressDialog;

        public AsyncFetchAirlines(Context context,ProgressDialog progressDialog) {
            this.context = context;
            this.progressDialog = progressDialog;
        }

        @Override
        protected Void doInBackground(String... strings) {
            MyRetrofitService myRetrofitService = MyRetrofitClient.getClient();
            Call<List<Airline>> call = myRetrofitService.selectAirLines();

            call.enqueue(new Callback<List<Airline>>() {
                @Override
                public void onResponse(Call<List<Airline>> call, Response<List<Airline>> response) {
                    CommonMethods.lst_airlines = (List<Airline>) response.body();
                    Log.e(TAG,"SplashActivity :: "+CommonMethods.lst_airlines.size());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        CommonMethods.fetchAirLineNames();
                        Log.e(TAG,"SplashActivity :: "+CommonMethods.lst_airlines_name.size());
                    }
                }

                @Override
                public void onFailure(Call<List<Airline>> call, Throwable t) {
                    Log.e(TAG,"SplashActivity :: Failed to get airlines data"+t.toString());
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            airline_adapter_name.notifyDataSetChanged();
            progressDialog.dismiss();
        }
    }

}

