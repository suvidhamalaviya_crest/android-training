package com.example.passanger.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.passanger.R;
import com.example.passanger.models.Airline;
import com.example.passanger.utils.CommonMethods;
import com.example.passanger.utils.DownloadImageUrl;

import java.util.List;

public class AirLinesAdapter extends RecyclerView.Adapter<AirLinesAdapter.ViewHolder> {

    Context context;
    List<Airline> lst_airlines;

    public AirLinesAdapter(Context context, List<Airline> lst_airlines) {
        this.context = context;
        this.lst_airlines = lst_airlines;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.airline_tile, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Airline current_airline = lst_airlines.get(position);
        holder.tv_name.setText(current_airline.getName());
        holder.tv_country.setText(current_airline.getCountry());
        holder.tv_slogan.setText(current_airline.getSlogan());
        String imageUrl = current_airline.getLogo();

        SpannableString content = new SpannableString(current_airline.getWebsite());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.tv_website.setText(content);

        Glide.with(context)
                .load(imageUrl)
                .into(holder.iv_profile);

        holder.iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission(current_airline.getLogo());
            }
        });

    }

    private void checkPermission(String url) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
            CommonMethods.showToast(context,"Need Permission to access storage for Downloading Image",R.drawable.ic_error);
        } else {
            CommonMethods.showToast(context,"Downloading Image...",R.drawable.ic_download);
            //Asynctask to create a thread to downlaod image in the background
            new DownloadImageUrl(context).execute(url);
        }
    }


    @Override
    public int getItemCount() {
        return lst_airlines.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_country, tv_slogan, tv_website;
        ImageView iv_profile,iv_download;
        CardView cv_airline;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_country = itemView.findViewById(R.id.tv_country);
            tv_slogan = itemView.findViewById(R.id.tv_slogan);
            tv_website = itemView.findViewById(R.id.tv_website);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            iv_download = itemView.findViewById(R.id.iv_download);
            cv_airline = itemView.findViewById(R.id.cv_airline);
        }
    }

}

