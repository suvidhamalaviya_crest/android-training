package com.example.passanger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.passanger.R;
import com.example.passanger.models.Datum;
import com.example.passanger.utils.PassangerOnClickListener;

import java.util.List;

public class PassangerAdapter extends RecyclerView.Adapter<PassangerAdapter.ViewHolder> {

    Context context;
    List<Datum> lst_passanger;
    PassangerOnClickListener passangerOnClickListener;

    public PassangerAdapter(Context context, List<Datum> lst_passanger, PassangerOnClickListener passangerOnClickListener) {
        this.context = context;
        this.lst_passanger = lst_passanger;
        this.passangerOnClickListener = passangerOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.passanger_tile,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Datum current_passanger = lst_passanger.get(position);

        holder.tv_name.setText(current_passanger.getName());
        holder.tv_trips.setText((current_passanger.getTrips()!=null)?"Trips : "+String.valueOf(current_passanger.getTrips()):"Trips : 0");

        holder.cv_passanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passangerOnClickListener.onPassangerClick(current_passanger,"View");
            }
        });
    }

    @Override
    public int getItemCount() {
        return lst_passanger.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tv_name,tv_trips;
        ImageView btn_edit,btn_delete,btn_copy,btn_share;
        CardView cv_passanger;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_trips = itemView.findViewById(R.id.tv_trips);
            btn_edit = itemView.findViewById(R.id.btn_edit);
            btn_delete = itemView.findViewById(R.id.btn_delete);
            btn_copy = itemView.findViewById(R.id.btn_copy);
            btn_share = itemView.findViewById(R.id.btn_share);
            cv_passanger = itemView.findViewById(R.id.cv_passanger);
        }
    }

}
