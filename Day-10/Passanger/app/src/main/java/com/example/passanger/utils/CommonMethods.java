package com.example.passanger.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.passanger.R;
import com.example.passanger.models.Airline;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommonMethods
{

    public static List<Airline> lst_airlines = new ArrayList<>();
    public static List<String> lst_airlines_name = new ArrayList<>();

    public static void showToast(Context context, String msg, int drawable){
        TextView custom_toast_message;
        ImageView custom_toast_image;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View layout = inflater.inflate(R.layout.custom_toast,null);
        custom_toast_message = layout.findViewById(R.id.custom_toast_message);
        custom_toast_image = layout.findViewById(R.id.custom_toast_image);
        custom_toast_message.setText(msg);
        custom_toast_image.setImageDrawable(context.getDrawable(drawable));

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void fetchAirLineNames(){
        lst_airlines_name = lst_airlines.stream().map(airline -> airline.getName()).collect(Collectors.toList());

    }

}
