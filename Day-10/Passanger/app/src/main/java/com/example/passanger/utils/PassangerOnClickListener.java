package com.example.passanger.utils;

import com.example.passanger.models.Datum;

public interface PassangerOnClickListener {
    void onPassangerClick(Datum passanger, String myAction);
}
