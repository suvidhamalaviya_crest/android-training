package com.example.passanger.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.passanger.R;
import com.example.passanger.adapter.AirLinesAdapter;
import com.example.passanger.models.Datum;

public class DetailViewActivity extends AppCompatActivity {

    Context mContext;
    TextView tv_name,tv_trip;
    RecyclerView rv_airline;
    Datum current_passanger;

    AirLinesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail_view);

        initComponent();

        if(getIntent().getExtras()!=null){
            current_passanger = (Datum) getIntent().getSerializableExtra("passenger");
        }

        setData();
    }

    private void initComponent() {
        mContext = this;
        tv_name = findViewById(R.id.tv_name);
        tv_trip = findViewById(R.id.tv_trip);
        rv_airline = findViewById(R.id.rv_airline);
    }

    private void setData() {
        tv_name.setText(current_passanger.getName());
        tv_trip.setText((current_passanger.getTrips()!=null)?"Trips : "+String.valueOf(current_passanger.getTrips()):"Trips : 0");

        rv_airline.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new AirLinesAdapter(this, current_passanger.getAirline());
        rv_airline.setAdapter(adapter);
    }

}