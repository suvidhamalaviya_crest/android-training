package com.example.profiler.Screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.profiler.MainActivity;
import com.example.profiler.Model.User;
import com.example.profiler.R;
import com.google.android.material.divider.MaterialDivider;

import java.util.Calendar;

public class AddUserActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener{

    EditText edt_name,edt_bio,edt_date,edt_msg_length;
    CheckBox chk_isnew;
    Button btn_add;
    TextView tv_user_title;
    MaterialDivider divider;

    Animation top_animation,middle_animation,bottom_animation;

    User user;

    int year,month,day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = AddUserActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(AddUserActivity.this, R.color.colorPrimaryVariant));
        setContentView(R.layout.activity_add_user);

        initComponent();

        setAnimation();
    }

    private void initComponent() {
        edt_name = findViewById(R.id.edt_name);
        edt_bio = findViewById(R.id.edt_bio);
        edt_date = findViewById(R.id.edt_date);
        edt_msg_length = findViewById(R.id.edt_msg_length);
        chk_isnew = findViewById(R.id.chk_isnew);
        btn_add = findViewById(R.id.btn_add);
        tv_user_title = findViewById(R.id.tv_user_title);
        divider = findViewById(R.id.divider);

        btn_add.setOnClickListener(this);
        edt_date.setOnClickListener(this);

        top_animation = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        middle_animation = AnimationUtils.loadAnimation(this,R.anim.middle_animation);
        bottom_animation = AnimationUtils.loadAnimation(this,R.anim.botton_animation);
    }

    private void setAnimation() {
        tv_user_title.setAnimation(top_animation);
        divider.setAnimation(middle_animation);
        edt_name.setAnimation(bottom_animation);
        edt_bio.setAnimation(bottom_animation);
        edt_date.setAnimation(bottom_animation);
        edt_msg_length.setAnimation(bottom_animation);
        chk_isnew.setAnimation(bottom_animation);
        btn_add.setAnimation(bottom_animation);
    }

    @Override
    public void onClick(View view) {

        Drawable error_icon = getResources().getDrawable(R.drawable.ic_error);
        error_icon.setBounds(0, 0, error_icon.getIntrinsicWidth(), error_icon.getIntrinsicHeight());

        if(view == btn_add){
            String name = edt_name.getText().toString();
            String bio = edt_bio.getText().toString();
            String date = edt_date.getText().toString();
            String msg_length = edt_msg_length.getText().toString();
            boolean status = chk_isnew.isChecked();
            Log.e("Status", String.valueOf(status));

            if(name.isEmpty()){
                edt_name.setError("Required",error_icon);
                return;
            }
            else if(bio.isEmpty()){
                edt_bio.setError("Required",error_icon);
                return;
            }
            else if(date.isEmpty()){
                edt_date.setError("Required",error_icon);
                return;
            }
            else
            {
                if(msg_length.isEmpty())
                    msg_length="0";
                else if(status && Integer.parseInt(msg_length)<=0)
                    msg_length="1";

                user = new User(name,bio,date,status,Integer.parseInt(msg_length));

                MainActivity.lst_user.add(0,user);

                finish();
            }
        }
        else if(view == edt_date)
        {
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(AddUserActivity.this, AddUserActivity.this,year, month,day);
            datePickerDialog.show();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        edt_date.setText(String.valueOf(month)+"/"+String.valueOf(day)+"/"+String.valueOf(year));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddUserActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}