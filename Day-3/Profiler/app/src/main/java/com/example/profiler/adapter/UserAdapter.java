package com.example.profiler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.profiler.Model.User;
import com.example.profiler.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    List<User> lst_user;
    Context context;

    public UserAdapter(Context context, List<User> lst_user) {
        this.lst_user = lst_user;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.profile_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name.setText(lst_user.get(position).getUserName());
        holder.tv_bio.setText(lst_user.get(position).getUserBio());
        holder.tv_date.setText(lst_user.get(position).getUserDate());
        if (lst_user.get(position).isIsNew()){
            holder.tv_count.setVisibility(View.VISIBLE);
            holder.tv_count.setText(String.valueOf(lst_user.get(position).getNewMsgLength()));

            holder.tv_date.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }
    }

    @Override
    public int getItemCount() {
        return lst_user.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_bio, tv_date, tv_count;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_bio = itemView.findViewById(R.id.tv_bio);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_count = itemView.findViewById(R.id.tv_count);
        }
    }
}
