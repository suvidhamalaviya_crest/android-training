package com.example.profiler.Model;

public class User
{
    private String user_name;
    private String user_bio;
    private String user_date;
    private boolean is_new;
    private int new_msg_length;

    public User(String user_name, String user_bio, String user_date, boolean is_new, int new_msg_length) {
        this.user_name = user_name;
        this.user_bio = user_bio;
        this.user_date = user_date;
        this.is_new = is_new;
        this.new_msg_length = new_msg_length;
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserBio() {
        return user_bio;
    }

    public void setUserBio(String user_bio) {
        this.user_bio = user_bio;
    }

    public String getUserDate() {
        return user_date;
    }

    public void setUserDate(String user_date) {
        this.user_date = user_date;
    }

    public boolean isIsNew() {
        return is_new;
    }

    public void setIsNew(boolean is_new) {
        this.is_new = is_new;
    }

    public int getNewMsgLength() {
        return new_msg_length;
    }

    public void setNewMsgLength(int new_msg_length) {
        this.new_msg_length = new_msg_length;
    }
}
