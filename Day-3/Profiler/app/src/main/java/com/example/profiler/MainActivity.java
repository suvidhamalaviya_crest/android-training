package com.example.profiler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.profiler.Model.User;
import com.example.profiler.Screens.AddUserActivity;
import com.example.profiler.adapter.UserAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv_users;
    FloatingActionButton fb_add;
    public static List<User> lst_user;
    UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = MainActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryVariant));
        setContentView(R.layout.activity_main);

        initComponent();

        setAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyItemInserted(0);
        //adapter.notifyDataSetChanged();
    }

    private void initComponent() {
        rv_users = findViewById(R.id.rv_users);
        fb_add = findViewById(R.id.fb_add);
        lst_user = new ArrayList<>();

        fb_add.setOnClickListener(this);

        lst_user.add(new User("Suvidha","Hi I am using Profiler!!","1/31/22",false,0));
        lst_user.add(new User("Tejas","Good Morning!!","Yesterday",false,0));
        lst_user.add(new User("Priyanka","Find The Fire!!","11:52 AM",true,3));
        lst_user.add(new User("Harshil","Be Free...","1:12 PM",false,0));
        lst_user.add(new User("Ekta","Busy","3:49 PM",true,1));
    }

    private void setAdapter() {
        Collections.reverse(lst_user);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        rv_users.setLayoutManager(linearLayoutManager);
        rv_users.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new UserAdapter(this,lst_user);
        rv_users.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if(view == fb_add){
            Intent intent = new Intent(MainActivity.this, AddUserActivity.class);
            startActivity(intent);
        }
    }
}