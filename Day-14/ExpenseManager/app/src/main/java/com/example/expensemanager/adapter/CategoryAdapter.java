package com.example.expensemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.expensemanager.R;
import com.example.expensemanager.models.Category;

import java.util.List;

public class CategoryAdapter extends ArrayAdapter<Category> {

    public CategoryAdapter(Context context,
                            List<Category> lstCategory)
    {
        super(context, 0, lstCategory);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_tile, parent, false);
        }

        Category currentItem = getItem(position);

        TextView tv_title = (TextView) convertView.findViewById(R.id.tv_title);
        tv_title.setText(currentItem.getCategoryName());

        ImageView iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);
        iv_icon.setImageResource(currentItem.getImageId());

        return convertView;
    }
}
