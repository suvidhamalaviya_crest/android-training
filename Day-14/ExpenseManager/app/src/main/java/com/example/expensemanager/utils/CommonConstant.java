package com.example.expensemanager.utils;

import com.example.expensemanager.R;

import java.util.ArrayList;
import java.util.List;

public interface CommonConstant
{
    String table_name = "tb_expense";
    String column_id = "ex_id";
    String column_date = "ex_date";
    String column_amount = "ex_amount";
    String column_type = "ex_type";
    String column_category = "ex_category";
    String column_note = "ex_note";

    String select_all_query = "select * from "+table_name;
    String select_sum_expences = "select SUM("+column_amount+") from "+table_name+" where "+column_type+" = 'Expense'";
    String select_sum_income = "select SUM("+column_amount+") from "+table_name+" where "+column_type+" = 'Income'";

}
