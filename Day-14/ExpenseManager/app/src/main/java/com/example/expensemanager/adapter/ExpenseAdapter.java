package com.example.expensemanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.expensemanager.R;
import com.example.expensemanager.models.Expense;
import com.example.expensemanager.screens.AddExpenseActivity;
import com.example.expensemanager.utils.OnClickOnExpense;

import java.util.List;

public class ExpenseAdapter extends ListAdapter<Expense, ExpenseAdapter.ViewHolder> {

    Context context;
    List<Expense> lstExpense;
    OnClickOnExpense onClickOnExpense;

    private static final DiffUtil.ItemCallback<Expense> DIFF_CALLBACK = new DiffUtil.ItemCallback<Expense>() {
        @Override
        public boolean areItemsTheSame(Expense oldItem, Expense newItem) {
            return oldItem.getEx_id() == newItem.getEx_id();
        }

        @Override
        public boolean areContentsTheSame(Expense oldItem, Expense newItem) {
            return oldItem.getEx_date().equals(newItem.getEx_date()) &&
                    oldItem.getEx_note().equals(newItem.getEx_note()) &&
                    oldItem.getEx_type().equals(newItem.getEx_type()) &&
                    oldItem.getEx_category().equals(newItem.getEx_category()) &&
                    oldItem.getEx_amount() == newItem.getEx_amount();
        }
    };


    public ExpenseAdapter(Context context, List<Expense> lstExpense,OnClickOnExpense onClickOnExpense) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.lstExpense = lstExpense;
        this.onClickOnExpense = onClickOnExpense;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.expense_tile,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tv_date.setText(lstExpense.get(position).getEx_date());
        holder.tv_amount.setText(context.getString(R.string.rs)+". "+String.valueOf(lstExpense.get(position).getEx_amount()));
        holder.tv_note.setText(lstExpense.get(position).getEx_note());
        holder.iv_category.setImageResource(getImageID(lstExpense.get(position).getEx_category()));

        if(lstExpense.get(position).getEx_type().equals("Income")){
            holder.ll_expense.setBackgroundColor(context.getColor(R.color.lightGreen));
        }

        /*holder.cv_expense.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                holder.ll_action.setVisibility(View.VISIBLE);
                return false;
            }
        });*/

        holder.cv_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddExpenseActivity.class);
                intent.putExtra("operation","Edit");
                intent.putExtra("expense",lstExpense.get(position));
                context.startActivity(intent);
            }
        });

    }

    public Expense getExpenseAt(int position) {
        return lstExpense.get(position);
    }

    private int getImageID(String category) {
        if (category.equals(context.getString(R.string.other)))
                return R.drawable.other;
        else if(category.equals(context.getString(R.string.travelling)))
            return R.drawable.travelling;
        else if(category.equals(context.getString(R.string.food)))
            return R.drawable.dining;
        else if(category.equals(context.getString(R.string.shopping)))
            return R.drawable.shopping;
        else if(category.equals(context.getString(R.string.medical)))
            return R.drawable.medical;
        else if(category.equals(context.getString(R.string.education)))
            return R.drawable.education;
        else if(category.equals(context.getString(R.string.inverstment)))
            return R.drawable.investment;
        else if(category.equals(context.getString(R.string.rent)))
            return R.drawable.rent;
        else
            return R.drawable.donation;
    }

    @Override
    public int getItemCount() {
        return lstExpense.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView iv_category;
        TextView tv_amount,tv_note,tv_date;
        LinearLayout ll_action,ll_expense;
        CardView cv_edit,cv_delete,cv_expense;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            iv_category = itemView.findViewById(R.id.iv_category);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_note = itemView.findViewById(R.id.tv_note);
            tv_date = itemView.findViewById(R.id.tv_date);
            ll_action = itemView.findViewById(R.id.ll_action);
            ll_expense = itemView.findViewById(R.id.ll_expense);
            cv_edit = itemView.findViewById(R.id.cv_edit);
            cv_delete = itemView.findViewById(R.id.cv_delete);
            cv_expense = itemView.findViewById(R.id.cv_expense);
        }
    }
}
