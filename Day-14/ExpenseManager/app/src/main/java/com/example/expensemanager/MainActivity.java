package com.example.expensemanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.expensemanager.adapter.ExpenseAdapter;
import com.example.expensemanager.database.ExpenseDatabase;
import com.example.expensemanager.models.Expense;
import com.example.expensemanager.screens.AddExpenseActivity;
import com.example.expensemanager.utils.CommonMethods;
import com.example.expensemanager.utils.OnClickOnExpense;
import com.example.expensemanager.utils.ViewModal;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnClickOnExpense {

    private static final String TAG = "ExpenseManager";
    Context mContext;
    TextView tv_total_spending,tv_total_income;
    RecyclerView rv_expenses;
    FloatingActionButton fb_add_expense;
    BarChart chart_bar;

    ExpenseDatabase database;
    int totalExpense,totalIncome;
    ExpenseAdapter adapter;
    AlertDialog.Builder builder;
    ViewModal viewmodal;

    ArrayList<BarEntry> lstExpenses = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        initComponent();
    }

    private void initComponent() {
        mContext = this;
        tv_total_spending = findViewById(R.id.tv_total_spending);
        tv_total_income = findViewById(R.id.tv_total_income);
        rv_expenses = findViewById(R.id.rv_expenses);
        fb_add_expense = findViewById(R.id.fb_add_expense);
        chart_bar = findViewById(R.id.chart_bar);

        fb_add_expense.setOnClickListener(this);
        database = ExpenseDatabase.getInstance(this);

        setAdapter();
    }


    private void setAdapter() {
        viewmodal = ViewModelProviders.of(this).get(ViewModal.class);

        viewmodal.selectAllExpenses().observe(this, lstExpense -> {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            rv_expenses.setLayoutManager(linearLayoutManager);
            adapter = new ExpenseAdapter(mContext,lstExpense,this);
            rv_expenses.setAdapter(adapter);

            totalExpense = viewmodal.getTotalExpense();
            totalIncome = viewmodal.getTotalIncome();

            lstExpenses.clear();
            for (int i=0;i<lstExpense.size();i++){
                lstExpenses.add(new BarEntry(Integer.parseInt(lstExpense.get(i).getEx_date().split("/")[0]), (float) lstExpense.get(i).getEx_amount()));
            }

            BarDataSet barDataSet = new BarDataSet(lstExpenses,"Expenses");
            barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
            barDataSet.setValueTextColor(R.color.black);
            barDataSet.setValueTextSize(16f);

            BarData barData = new BarData(barDataSet);
            barData.setBarWidth(0.2f);

            chart_bar.setFitBars(true);
            chart_bar.setData(barData);
            chart_bar.getXAxis().setLabelCount(31);
            chart_bar.getXAxis().setAxisMinimum(1);
            chart_bar.getXAxis().setAxisMaximum(31);

            tv_total_spending.setText(String.valueOf(totalExpense));
            tv_total_income.setText(String.valueOf(totalIncome));
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Are you sure to delete the item?")
                        .setTitle("Confirmation")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int pos = viewHolder.getAdapterPosition();
                                Log.e("Position",String.valueOf(pos));
                                viewmodal.delete(adapter.getExpenseAt(pos));
                                CommonMethods.showToast(mContext, getString(R.string.delete_success));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            }
        }).attachToRecyclerView(rv_expenses);

    }

    @Override
    public void onClick(View view) {
        if(view == fb_add_expense){
            Intent intent = new Intent(mContext, AddExpenseActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onExpenseClick(Expense expense, String action) {
        if(action.equals("Edit")){
            Intent intent = new Intent(mContext,AddExpenseActivity.class);
            intent.putExtra("operation","Edit");
            intent.putExtra("expense",expense);
            startActivity(intent);
        }else{
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Are you sure to delete the item?")
                    .setTitle("Confirmation")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            viewmodal.delete(expense);
                            CommonMethods.showToast(mContext, getString(R.string.delete_success));
                            //fetchData();
                            adapter.notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();

        }
    }
}