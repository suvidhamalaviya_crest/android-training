package com.example.expensemanager.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.expensemanager.models.Expense;
import com.example.expensemanager.utils.CommonConstant;

import java.util.List;

@Dao
public interface ExpenseDao {

    @Query(CommonConstant.select_all_query)
    LiveData<List<Expense>> getExpenseList();

    @Insert
    void insertExpense(Expense expense);

    @Update
    void updateExpense(Expense expense);

    @Delete
    void deleteExpense(Expense expense);

    @Query(CommonConstant.select_sum_expences)
    int getTotalExpense();

    @Query(CommonConstant.select_sum_income)
    int getTotalIncome();
}
