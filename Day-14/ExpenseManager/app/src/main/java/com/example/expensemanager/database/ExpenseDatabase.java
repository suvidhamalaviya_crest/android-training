package com.example.expensemanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.expensemanager.dao.ExpenseDao;
import com.example.expensemanager.models.Expense;

@Database(entities = {Expense.class},exportSchema = false,version = 1)
public abstract class ExpenseDatabase extends RoomDatabase {

    private static final String DB_NAME = "expense_manager";
    private static ExpenseDatabase INSTANCE;

    public static synchronized ExpenseDatabase getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ExpenseDatabase.class,DB_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public abstract ExpenseDao expenseDao();
}
