package com.example.expensemanager.utils;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.expensemanager.dao.ExpenseDao;
import com.example.expensemanager.database.ExpenseDatabase;
import com.example.expensemanager.models.Expense;

import java.util.List;

public class ExpenseRepository
{
    private ExpenseDao expenseDao;
    private LiveData<List<Expense>> lstExpense;
    private int totalExpense,totalIncome;

    public ExpenseRepository(Application application) {
        ExpenseDatabase database = ExpenseDatabase.getInstance(application);
        expenseDao = database.expenseDao();
        lstExpense = expenseDao.getExpenseList();
        totalExpense = expenseDao.getTotalExpense();
        totalIncome = expenseDao.getTotalIncome();
    }

    public void insert(Expense expense) {
        new InsertExpenseAsyncTask(expenseDao).execute(expense);
    }

    private static class InsertExpenseAsyncTask extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao dao;

        private InsertExpenseAsyncTask(ExpenseDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Expense... model) {
            dao.insertExpense(model[0]);
            return null;
        }
    }

    public void update(Expense model) {
        new UpdateExpenseAsyncTask(expenseDao).execute(model);
    }

    private static class UpdateExpenseAsyncTask extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao dao;

        private UpdateExpenseAsyncTask(ExpenseDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Expense... models) {
            dao.updateExpense(models[0]);
            return null;
        }
    }

    public void delete(Expense model) {
        new DeleteExpenseAsyncTask(expenseDao).execute(model);
    }

    private static class DeleteExpenseAsyncTask extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao dao;

        private DeleteExpenseAsyncTask(ExpenseDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Expense... models) {
            dao.deleteExpense(models[0]);
            return null;
        }
    }

    public LiveData<List<Expense>> getAllExpenses() {
        return lstExpense;
    }

    public int getTotalExpense(){
        return totalExpense;
    }

    public int getTotalIncome(){
        return totalIncome;
    }
}
