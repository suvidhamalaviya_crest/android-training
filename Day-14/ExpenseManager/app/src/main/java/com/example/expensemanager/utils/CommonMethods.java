package com.example.expensemanager.utils;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.expensemanager.R;

public class CommonMethods
{

    public static void showToast(Context context,String msg){
        TextView custom_toast_message;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View layout = inflater.inflate(R.layout.custom_toast,null);
        custom_toast_message = layout.findViewById(R.id.custom_toast_message);
        custom_toast_message.setText(msg);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();

    }

}
