package com.example.expensemanager.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.expensemanager.utils.CommonConstant;

import java.io.Serializable;

@Entity(tableName = CommonConstant.table_name)
public class Expense implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = CommonConstant.column_id)
    private int ex_id;

    @ColumnInfo(name = CommonConstant.column_date)
    private String ex_date;

    @ColumnInfo(name = CommonConstant.column_amount)
    private double ex_amount;

    @ColumnInfo(name = CommonConstant.column_type)
    private String ex_type;

    @ColumnInfo(name = CommonConstant.column_category)
    private String ex_category;

    @ColumnInfo(name = CommonConstant.column_note)
    private String ex_note;

    public Expense(int ex_id, String ex_date, double ex_amount, String ex_type, String ex_category, String ex_note) {
        this.ex_id = ex_id;
        this.ex_date = ex_date;
        this.ex_amount = ex_amount;
        this.ex_type = ex_type;
        this.ex_category = ex_category;
        this.ex_note = ex_note;
    }

    @Ignore
    public Expense(String ex_date, double ex_amount, String ex_type, String ex_category, String ex_note) {
        this.ex_date = ex_date;
        this.ex_amount = ex_amount;
        this.ex_type = ex_type;
        this.ex_category = ex_category;
        this.ex_note = ex_note;
    }

    public int getEx_id() {
        return ex_id;
    }

    public void setEx_id(int ex_id) {
        this.ex_id = ex_id;
    }

    public String getEx_date() {
        return ex_date;
    }

    public void setEx_date(String ex_date) {
        this.ex_date = ex_date;
    }

    public double getEx_amount() {
        return ex_amount;
    }

    public void setEx_amount(double ex_amount) {
        this.ex_amount = ex_amount;
    }

    public String getEx_type() {
        return ex_type;
    }

    public void setEx_type(String ex_type) {
        this.ex_type = ex_type;
    }

    public String getEx_category() {
        return ex_category;
    }

    public void setEx_category(String ex_category) {
        this.ex_category = ex_category;
    }

    public String getEx_note() {
        return ex_note;
    }

    public void setEx_note(String ex_note) {
        this.ex_note = ex_note;
    }
}
