package com.example.expensemanager.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.example.expensemanager.MainActivity;
import com.example.expensemanager.R;
import com.example.expensemanager.adapter.CategoryAdapter;
import com.example.expensemanager.database.ExpenseDatabase;
import com.example.expensemanager.models.Category;
import com.example.expensemanager.models.Expense;
import com.example.expensemanager.utils.CommonMethods;
import com.example.expensemanager.utils.ViewModal;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddExpenseActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    Context mContext;
    TextInputEditText edt_date,edt_amount,edt_note;
    RadioButton rb_expense,rb_income;
    Spinner sp_category;
    FloatingActionButton fb_add_expense;
    CategoryAdapter categoryAdapter;

    private ViewModal viewmodal;

    String operation="";
    Expense expense;

    List<Category> lstCategory = new ArrayList<>();
    int day,month,year;

    ViewModal viewModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_expense);

        initComponent();

        if(getIntent().getExtras()!=null){
            operation = getIntent().getStringExtra("operation");
            expense = (Expense) getIntent().getSerializableExtra("expense");
            setData();
        }

    }

    private void setData() {
        edt_date.setText(expense.getEx_date());
        edt_note.setText(expense.getEx_note());
        edt_amount.setText(String.valueOf(expense.getEx_amount()));
        if (expense.getEx_type().equals("Income")) {
            rb_income.setChecked(true);
        } else {
            rb_expense.setChecked(true);
        }
    }

    private void initComponent() {

        mContext = this;
        edt_date = findViewById(R.id.edt_date);
        edt_amount = findViewById(R.id.edt_amount);
        edt_note = findViewById(R.id.edt_note);
        rb_expense = findViewById(R.id.rb_expense);
        rb_income = findViewById(R.id.rb_income);
        sp_category = findViewById(R.id.sp_category);
        fb_add_expense = findViewById(R.id.fb_add_expense);

        Calendar calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DATE);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        edt_date.setText(day+"/"+month+"/"+year);

        fb_add_expense.setOnClickListener(this);
        edt_date.setOnClickListener(this);
        viewmodal = ViewModelProviders.of(this).get(ViewModal.class);

        /*viewmodal.getAllCourses().observe(this, new Observer<List<CourseModal>>() {
            @Override
            public void onChanged(List<CourseModal> models) {
                // when the data is changed in our models we are
                // adding that list to our adapter class.
                adapter.submitList(models);
            }
        });*/

        setCategoryData();

        categoryAdapter = new CategoryAdapter(mContext,lstCategory);
        sp_category.setAdapter(categoryAdapter);
    }

    private void setCategoryData() {
        lstCategory.add(new Category(getString(R.string.other),R.drawable.other));
        lstCategory.add(new Category(getString(R.string.travelling),R.drawable.travelling));
        lstCategory.add(new Category(getString(R.string.food),R.drawable.dining));
        lstCategory.add(new Category(getString(R.string.shopping),R.drawable.shopping));
        lstCategory.add(new Category(getString(R.string.medical),R.drawable.medical));
        lstCategory.add(new Category(getString(R.string.education),R.drawable.education));
        lstCategory.add(new Category(getString(R.string.inverstment),R.drawable.investment));
        lstCategory.add(new Category(getString(R.string.rent),R.drawable.rent));
        lstCategory.add(new Category(getString(R.string.donation),R.drawable.donation));
    }

    @Override
    public void onClick(View view) {
        if(view == fb_add_expense){
            String date = edt_date.getText().toString().trim();
            String amount = edt_amount.getText().toString().trim();
            String note = edt_note.getText().toString().trim();
            String type = (rb_expense.isChecked())?rb_expense.getText().toString():rb_income.getText().toString();
            String category = ((Category)sp_category.getSelectedItem()).getCategoryName();

            if(date.isEmpty()){
                edt_date.setError("Required");
                return;
            }
            else if(amount.isEmpty()){
                edt_amount.setError("Required");
                return;
            }
            else if(note.isEmpty()){
                edt_note.setError("Required");
                return;
            }
            else{
                if(operation.equals("Edit")){
                    Expense current_expense = new Expense(expense.getEx_id(),date,Double.parseDouble(amount),type,category,note);
                    viewmodal.update(current_expense);
                    CommonMethods.showToast(mContext,getString(R.string.expense_updated));
                }
                else{
                    Expense current_expense = new Expense(date,Double.parseDouble(amount),type,category,note);
                    viewmodal.insert(current_expense);
                    CommonMethods.showToast(mContext,getString(R.string.expense_added));
                }
                finish();
            }
        }
        else if(view == edt_date){
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,this,year,month,day);
            datePickerDialog.show();

        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        edt_date.setText(d+"/"+m+"/"+y);
    }
}