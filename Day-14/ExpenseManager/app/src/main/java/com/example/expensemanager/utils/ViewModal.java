package com.example.expensemanager.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.expensemanager.models.Expense;

import java.util.List;

public class ViewModal extends AndroidViewModel {

    private ExpenseRepository expenseRepository;
    private LiveData<List<Expense>> lstExpenses;
    private int totalExpense,totalIncome;

    public ViewModal(@NonNull Application application) {
        super(application);
        expenseRepository = new ExpenseRepository(application);
        lstExpenses = expenseRepository.getAllExpenses();
        totalExpense = expenseRepository.getTotalExpense();
        totalIncome = expenseRepository.getTotalIncome();
    }

    public void insert(Expense model) {
        expenseRepository.insert(model);
    }

    public void update(Expense model) {
        expenseRepository.update(model);
    }

    public void delete(Expense model) {
        expenseRepository.delete(model);
    }

    public LiveData<List<Expense>> selectAllExpenses() {
        return lstExpenses;
    }

    public int getTotalExpense(){
        return totalExpense;
    }

    public int getTotalIncome(){
        return totalIncome;
    }
}
