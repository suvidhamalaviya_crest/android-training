package com.example.expensemanager.utils;

import com.example.expensemanager.models.Expense;

public interface OnClickOnExpense {
    void onExpenseClick(Expense expense,String action);
}
