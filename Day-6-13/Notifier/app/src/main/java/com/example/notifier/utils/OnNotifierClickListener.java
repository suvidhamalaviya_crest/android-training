package com.example.notifier.utils;

import android.view.View;

import com.example.notifier.Model.Reminder;

public interface OnNotifierClickListener
{
    public void performAction(int position, Reminder rem, View view);
}
