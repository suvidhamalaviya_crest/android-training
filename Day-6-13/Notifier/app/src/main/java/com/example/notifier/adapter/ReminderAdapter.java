package com.example.notifier.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notifier.MainActivity;
import com.example.notifier.Model.Reminder;
import com.example.notifier.R;
import com.example.notifier.utils.OnNotifierClickListener;

import java.util.List;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ViewHolder>  {

    List<Reminder> lst_reminder;
    Context context;
    OnNotifierClickListener onNotifierClickListener;

    public ReminderAdapter(Context context, List<Reminder> lst_reminder,OnNotifierClickListener onNotifierClickListener) {
        this.lst_reminder = lst_reminder;
        this.context = context;
        this.onNotifierClickListener = onNotifierClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.reminder_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_title.setText(lst_reminder.get(position).getTitle());
        holder.tv_desc.setText(lst_reminder.get(position).getDescription());
        holder.tv_date.setText(lst_reminder.get(position).getDate());
        holder.tv_time.setText(lst_reminder.get(position).getTime());
        if (lst_reminder.get(position).isReceived()){
            holder.tv_status.setText("Received");
            holder.tv_status.setTextColor(context.getResources().getColor(R.color.green));
        }else{
            holder.tv_status.setText("Pending");
            holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
        }

        holder.cv_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNotifierClickListener.performAction(position,lst_reminder.get(position),view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lst_reminder.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_desc, tv_date, tv_time,tv_status;
        CardView cv_reminder;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_status = itemView.findViewById(R.id.tv_status);
            cv_reminder = itemView.findViewById(R.id.cv_reminder);



            //itemView.setOnCreateContextMenuListener((View.OnCreateContextMenuListener) context);
        }

       /* @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            menu.add(Menu.NONE, R.id.menu_edit,
                    Menu.NONE, "Edit");
            menu.add(Menu.NONE, R.id.menu_delete,
                    Menu.NONE, "Delete");
            menu.add(Menu.NONE, R.id.menu_share,
                    Menu.NONE, "Share");
        }*/
    }
}
