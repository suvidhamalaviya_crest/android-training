package com.example.notifier.utils;

import com.example.notifier.Model.Reminder;

import java.util.ArrayList;
import java.util.List;

public interface Constant
{
    public static int DB_VERSION = 1;
    public static String DB_NAME = "Notifier";
    public static String TABLE_REMINDERS = "Reminders";
    public static String KEY_ID = "id";
    public static String KEY_TITLE = "title";
    public static String KEY_DESC = "description";
    public static String KEY_DATE = "date";
    public static String KEY_TIME = "time";
    public static String KEY_RECEIVED = "received";

    String CREATE_REMINDERS_TABLE = "CREATE TABLE " + TABLE_REMINDERS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_TITLE + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_DATE + " TEXT,"
            + KEY_TIME + " TEXT,"
            + KEY_RECEIVED + " INTEGER )";

    String DELETE_REMINDERS_TABLE = "DROP TABLE IF EXISTS " + TABLE_REMINDERS;

    String SELECT_ALL_REMINDERS = "SELECT * FROM " + TABLE_REMINDERS;
}
