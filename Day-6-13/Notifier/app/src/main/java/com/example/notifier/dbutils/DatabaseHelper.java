package com.example.notifier.dbutils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.notifier.Model.Reminder;
import com.example.notifier.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context){
        super(context, Constant.DB_NAME,null,Constant.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Constant.CREATE_REMINDERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Constant.DELETE_REMINDERS_TABLE);

        onCreate(sqLiteDatabase);
    }

    public boolean addReminder(Reminder reminder){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.KEY_TITLE,reminder.getTitle());
        contentValues.put(Constant.KEY_DESC,reminder.getDescription());
        contentValues.put(Constant.KEY_DATE,"");
        contentValues.put(Constant.KEY_TIME,reminder.getTime());
        contentValues.put(Constant.KEY_RECEIVED,false);

        long isInserted = db.insert(Constant.TABLE_REMINDERS,null,contentValues);
        db.close();

        return isInserted != -1;
    }

    public boolean updateReminder(Reminder reminder){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.KEY_TITLE,reminder.getTitle());
        contentValues.put(Constant.KEY_DESC,reminder.getDescription());
        contentValues.put(Constant.KEY_DATE,"");
        contentValues.put(Constant.KEY_TIME,reminder.getTime());
        contentValues.put(Constant.KEY_RECEIVED,reminder.isReceived());

        long isUpdated = db.update(Constant.TABLE_REMINDERS,contentValues,Constant.KEY_ID+"=?",new String[]{String.valueOf(reminder.getId())});
        db.close();

        return isUpdated != -1;
    }

    public boolean deleteReminder(Reminder reminder){
        SQLiteDatabase db = this.getWritableDatabase();

        long isDeleted = db.delete(Constant.TABLE_REMINDERS,Constant.KEY_ID+"=?",new String[]{String.valueOf(reminder.getId())});
        db.close();
        Log.e("Notifier","DatabaseHelper: IsDeleted: "+isDeleted);
        return isDeleted != -1;
    }

    public List<Reminder> getAllReminder(){
        List<Reminder> lstReminder = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Constant.SELECT_ALL_REMINDERS,null);

        if (cursor.moveToFirst()){
            do {
                Reminder reminder = new Reminder();
                reminder.setId(cursor.getInt(0));
                reminder.setTitle(cursor.getString(1));
                reminder.setDescription(cursor.getString(2));
                reminder.setDate(cursor.getString(3));
                reminder.setTime(cursor.getString(4));
                reminder.setReceived(cursor.getInt(5) != 0);

                lstReminder.add(reminder);
            }while (cursor.moveToNext());
            Log.e("Notifier","DatabaseHelper: Size : "+lstReminder.size());
        }
        
        cursor.close();

        return lstReminder;
    }

    Reminder getReminder(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Constant.TABLE_REMINDERS, new String[] { Constant.KEY_ID,
                        Constant.KEY_TITLE, Constant.KEY_DESC,Constant.KEY_TIME }, Constant.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Reminder reminder = null;
        if (cursor != null) {
            reminder = new Reminder(cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4), cursor.getInt(5) != 0);
        }

        return reminder;
    }
}
