package com.example.notifier.Model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Reminder implements Serializable,Comparable<Reminder>
{
    private int id;
    private String title;
    private String description;
    private String date;
    private String time;
    private boolean isReceived;

    public Reminder(){

    }

    public Reminder(String title, String description, String date, String time, boolean isReceived) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.time = time;
        this.isReceived = isReceived;
    }

    public Reminder(Reminder rem) {
        this.id = rem.getId();
        this.title = rem.getTitle();
        this.description = rem.getDescription();
        this.time = rem.getTime();
        this.date = rem.getDate();
        this.isReceived = rem.isReceived();
    }

    public Reminder(int id, String title, String description, String date, String time, boolean isReceived) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.time = time;
        this.isReceived = isReceived;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public void setReceived(boolean received) {
        isReceived = received;
    }

    @Override
    public int compareTo(Reminder reminder) {
        DateFormat f = new SimpleDateFormat("MM/dd/yyyy");
        try {
            return f.parse(getDate()).compareTo(f.parse(reminder.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
