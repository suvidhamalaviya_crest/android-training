package com.example.notifier;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;

import com.example.notifier.Model.Reminder;
import com.example.notifier.adapter.ReminderAdapter;
import com.example.notifier.dbutils.DatabaseHelper;
import com.example.notifier.screens.AddReminderActivity;
import com.example.notifier.utils.CommonMethods;
import com.example.notifier.utils.Constant;
import com.example.notifier.utils.OnNotifierClickListener;
import com.example.notifier.utils.SharedPrefUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnNotifierClickListener {

    private static final String TAG = "Notifier";
    RecyclerView rv_reminders;
    FloatingActionButton fb_add;
    Toolbar toolbar;

    ReminderAdapter adapter;

    AlertDialog.Builder builder;
    int pos;
    Reminder reminder;
    DatabaseHelper databaseHelper;
    List<Reminder> lst_reminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        initComponent();

        fetchData();

        setSupportActionBar(toolbar);

        setAdapter();

    }

    @Override
    protected void onResume() {
        super.onResume();
        rv_reminders.getRecycledViewPool().clear();
        fetchData();
        setAdapter();
    }

    private void fetchData() {
        lst_reminder = databaseHelper.getAllReminder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sortList();
        }
        Log.e(TAG,"MainActivity: Size "+lst_reminder.size());
    }

    private void initComponent() {
        rv_reminders = findViewById(R.id.rv_reminders);
        fb_add = findViewById(R.id.fb_add);
        toolbar = findViewById(R.id.toolbar);

        fb_add.setOnClickListener(this);
        databaseHelper = new DatabaseHelper(this);
        lst_reminder = new ArrayList<>();

        CommonMethods.createNotificationChannel(this);
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_reminders.setLayoutManager(linearLayoutManager);
        rv_reminders.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new ReminderAdapter(this, lst_reminder,this);
        rv_reminders.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view == fb_add) {
            Intent intent = new Intent(this, AddReminderActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void performAction(int position, Reminder rem,View view) {
        pos = position;
        Log.e(TAG,"MainActivity: "+rem.toString());
        reminder = new Reminder(rem);
        showMenu(view);
    }

    private void showMenu(View v){
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.main_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_edit) {
                    Intent intent = new Intent(MainActivity.this, AddReminderActivity.class);
                    intent.putExtra("operation", "Edit");
                    intent.putExtra("item", reminder);
                    intent.putExtra("position", String.valueOf(pos));
                    startActivity(intent);
                } else if (item.getItemId() == R.id.menu_delete) {
                    builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Are you sure to delete the item?")
                            .setTitle("Confirmation")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if(databaseHelper.deleteReminder(reminder)){
                                        rv_reminders.getRecycledViewPool().clear();
                                        fetchData();
                                        setAdapter();
                                        CommonMethods.showToast(MainActivity.this, "Deleted Successfully");
                                    }
                                    else{
                                        CommonMethods.showToast(MainActivity.this, "Failed To Delete");
                                    }
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                } else if (item.getItemId() == R.id.menu_share) {
                    Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Reminder");
                    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "=========================================="
                            + "Name : " + reminder.getTitle()
                            + "Description : " + reminder.getDescription()
                            + "Date : " + reminder.getDate()
                            + "Time : " + reminder.getTime()
                            + "==========================================");
                    startActivity(Intent.createChooser(shareIntent, "Share via"));
                } else if (item.getItemId() == R.id.menu_exit) {
                    System.exit(0);
                }
                return true;
            }
        });
        popup.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    void sortList(){
        Collections.sort(lst_reminder);

        Collections.sort(lst_reminder, new Comparator<Reminder>() {
            //DateFormat f1 = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat f2 = new SimpleDateFormat("hh:mm");
            @Override
            public int compare(Reminder reminder, Reminder t1) {
                try {
                    return f2.parse(reminder.getTime()).compareTo(f2.parse(t1.getTime()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });

    }
}