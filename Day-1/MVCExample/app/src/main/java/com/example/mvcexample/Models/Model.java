package com.example.mvcexample.Models;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Model extends Observable
{
    private List<Integer> lst_btn_count;


    /***
     * This method will initialize the list with initial click count
     */
    public Model() {
        lst_btn_count = new ArrayList<>(3);

        lst_btn_count.add(0);
        lst_btn_count.add(0);
        lst_btn_count.add(0);
    }

    /***
     * This method will return the specific count for the specific button
     * @param index : button position
     * @return : count of click
     * @throws IndexOutOfBoundsException
     */
    public int getValueForSpecificButton(int index) throws IndexOutOfBoundsException
    {
        return lst_btn_count.get(index);
    }

    /***
     * This method will increment the specific count for the specific button
     * @param index : button position
     * @throws IndexOutOfBoundsException
     */
    public void setValueForSpecificButton(int index) throws IndexOutOfBoundsException
    {
        lst_btn_count.set(index,lst_btn_count.get(index)+1);
        setChanged();
        notifyObservers();
    }
}
