package com.example.mvcexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mvcexample.Models.Model;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer, View.OnClickListener {

    private Model model;

    private Button btn_1,btn_2,btn_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
    }

    /***
     * This method will initialise the component of xml files as well as used variables and attach component with listeners
     */
    private void initComponent() {
        model = new Model();
        model.addObserver(this);

        btn_1 = findViewById(R.id.btn_1);
        btn_2 = findViewById(R.id.btn_2);
        btn_3 = findViewById(R.id.btn_3);

        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_1:
                model.setValueForSpecificButton(0);
                break;
            case R.id.btn_2:
                model.setValueForSpecificButton(1);
                break;
            case R.id.btn_3:
                model.setValueForSpecificButton(2);
                break;
        }
    }

    /***
     * This method will update the button count display when the button is clicked
     * @param observable
     * @param o
     */
    @Override
    public void update(Observable observable, Object o) {
        btn_1.setText("Count : "+model.getValueForSpecificButton(0));
        btn_2.setText("Count : "+model.getValueForSpecificButton(1));
        btn_3.setText("Count : "+model.getValueForSpecificButton(2));
    }
}