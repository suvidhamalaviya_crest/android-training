package com.example.newsfeed.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.models.NewsFeed;
import com.example.newsfeed.utils.CommonMethods;

import java.util.Calendar;

public class AddNewsActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "Notifier";
    EditText edt_title,edt_desc,edt_date,edt_time;
    Button btn_add_reminder;
    TextView tv_title;
    View divider;
    NewsFeed news;
    Spinner sp_category;
    int year,month,day,hour,minute;
    String operation = "";
    Animation top_animation,middle_animation,bottom_animation;
    ArrayAdapter adapter;
    String position="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_news);

        if(getIntent().getExtras()!=null) {
            operation = getIntent().getStringExtra("operation").toString();
            news = (NewsFeed) getIntent().getSerializableExtra("item");
            position = getIntent().getStringExtra("position").toString();
        }

        initComponent();
        setAnimation();

        if(operation!=null & operation.equals("Edit")){
            setData();
        }
    }

    private void setData() {
        edt_title.setText(news.getTitle());
        edt_desc.setText(news.getDescription());
        edt_date.setText(news.getDate());
        edt_time.setText(news.getTime());
        btn_add_reminder.setText("Update");
    }

    private void initComponent() {
        btn_add_reminder = findViewById(R.id.btn_add_reminder);
        edt_title = findViewById(R.id.edt_title);
        edt_desc = findViewById(R.id.edt_desc);
        edt_date = findViewById(R.id.edt_date);
        edt_time = findViewById(R.id.edt_time);
        tv_title = findViewById(R.id.tv_title);
        divider = findViewById(R.id.divider);
        sp_category = findViewById(R.id.sp_category);

        top_animation = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        middle_animation = AnimationUtils.loadAnimation(this,R.anim.middle_animation);
        bottom_animation = AnimationUtils.loadAnimation(this,R.anim.botton_animation);

        btn_add_reminder.setOnClickListener(this);
        edt_date.setOnClickListener(this);
        edt_time.setOnClickListener(this);

        adapter = new ArrayAdapter(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,MainActivity.lst_category);

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        sp_category.setAdapter(adapter);
    }

    private void setAnimation() {
        tv_title.setAnimation(top_animation);
        divider.setAnimation(middle_animation);
        edt_title.setAnimation(bottom_animation);
        edt_desc.setAnimation(bottom_animation);
        edt_date.setAnimation(bottom_animation);
        edt_time.setAnimation(bottom_animation);
        btn_add_reminder.setAnimation(bottom_animation);
    }

    @Override
    public void onClick(View view) {
        if(view == btn_add_reminder){
            String name = edt_title.getText().toString().trim();
            String desc = edt_desc.getText().toString().trim();
            String date = edt_date.getText().toString().trim();
            String time = edt_time.getText().toString().trim();
            String category = sp_category.getSelectedItem().toString();

            if(name.isEmpty()){
                CommonMethods.showToast(this,"Please Provide Title");
                return;
            }
            else if(desc.isEmpty()){
                CommonMethods.showToast(this,"Please Provide Description");
                return;
            }
            else if(date.isEmpty()){
                CommonMethods.showToast(this,"Please Provide Date");
                return;
            }
            else if(time.isEmpty()){
                CommonMethods.showToast(this,"Please Provide Time");
                return;
            }
            else{
                NewsFeed newsFeed = new NewsFeed(name,desc,date,time,category);
                if(operation.equals("Edit")){
                    MainActivity.lst_news.set(Integer.parseInt(position),newsFeed);
                    CommonMethods.showToast(this,"News Updated");
                }
                else{
                    MainActivity.lst_news.add(0,newsFeed);
                    CommonMethods.showToast(this,"News Added");
                }
                Log.e(TAG,"AddProductFragment"+MainActivity.lst_news.size());
                CommonMethods.filterList();
                finish();
            }
        }
        else if(view == edt_date){
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,year, month,day);
            datePickerDialog.show();
        }
        else if(view == edt_time){
            Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR);
            minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, this,hour, minute, DateFormat.is24HourFormat(this));
            timePickerDialog.show();
        }

        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        edt_date.setText(String.valueOf(month)+"/"+String.valueOf(day)+"/"+String.valueOf(year));
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int h, int m) {
        hour = h;
        minute = m;
        edt_time.setText(String.valueOf(h)+":"+String.valueOf(m));
    }
}