package com.example.newsfeed.utils;

public interface NewsHandlingListener {
    public void newsEdit(int position);
    public void newsDelete(int position);
}
