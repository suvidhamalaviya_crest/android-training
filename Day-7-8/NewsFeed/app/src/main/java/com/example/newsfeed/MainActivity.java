package com.example.newsfeed;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.newsfeed.adapter.PagerAdapter;
import com.example.newsfeed.models.NewsFeed;
import com.example.newsfeed.screens.AddCategoryActivity;
import com.example.newsfeed.screens.AddNewsActivity;
import com.example.newsfeed.utils.CommonMethods;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    ViewPager pager;
    TabLayout mTabLayout;
    TabItem item_all, item_education, item_covid;
    Toolbar toolbar;
    PagerAdapter adapter;

    public static List<String> lst_category;
    public static List<NewsFeed> lst_news,lst_edu,lst_covid;
    int pos;
    NewsFeed news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        initComponent();
    }

    private void initComponent() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayput);

        item_all = findViewById(R.id.item_all);
        item_education = findViewById(R.id.item_education);
        item_covid = findViewById(R.id.item_covid);

        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        adapter = new PagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,mTabLayout.getTabCount());
        pager.setAdapter(adapter);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        lst_category = new ArrayList<>();
        lst_news = new ArrayList<>();
        lst_edu = new ArrayList<>();
        lst_covid = new ArrayList<>();

        lst_category.add("Education");
        lst_category.add("Covid");
        CommonMethods.filterList();
    }



    @Override
    protected void onResume() {
        super.onResume();
        CommonMethods.filterList();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if(item.getItemId() == R.id.menu_home){

        }
        else if(item.getItemId() == R.id.menu_category){
            Intent intent = new Intent(this, AddCategoryActivity.class);
            //intent.putExtra("operation", "Add");
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.menu_add){
            Intent intent = new Intent(this, AddNewsActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.menu_exit){
            System.exit(0);
        }
        return false;
    }

}
