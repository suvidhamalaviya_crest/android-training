package com.example.newsfeed.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.adapter.NewsAdapter;
import com.example.newsfeed.utils.CommonMethods;
import com.example.newsfeed.utils.NewsHandlingListener;

public class CovidFragment extends Fragment implements NewsHandlingListener {

    private static final String TAG = "NewsFeed";
    RecyclerView rv_covid;
    NewsAdapter adapter;

    public CovidFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_covid, container, false);

        initComponent(view);

        setAdapter();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            CommonMethods.filterList();
            adapter.notifyDataSetChanged();
        }
    }

    private void initComponent(View view) {
        rv_covid = view.findViewById(R.id.rv_covid);
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_covid.setLayoutManager(linearLayoutManager);
        rv_covid.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter = new NewsAdapter(getContext(), MainActivity.lst_covid,this,false);
        rv_covid.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        CommonMethods.filterList();
    }

    @Override
    public void newsEdit(int position) {

    }

    @Override
    public void newsDelete(int position) {

    }
}