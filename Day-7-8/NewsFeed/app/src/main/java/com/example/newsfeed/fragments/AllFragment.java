package com.example.newsfeed.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.adapter.NewsAdapter;
import com.example.newsfeed.screens.AddNewsActivity;
import com.example.newsfeed.utils.CommonMethods;
import com.example.newsfeed.utils.NewsHandlingListener;

public class AllFragment extends Fragment implements NewsHandlingListener {

    private static final String TAG = "NewsFeed";
    RecyclerView rc_all;
    NewsAdapter adapter;
    AlertDialog.Builder builder;

    public AllFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all, container, false);

        initComponent(view);

        setAdapter();

        return view;
    }

    private void initComponent(View view) {
        rc_all = view.findViewById(R.id.rv_all);
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rc_all.setLayoutManager(linearLayoutManager);
        rc_all.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter = new NewsAdapter(getContext(), MainActivity.lst_news,this,true);
        rc_all.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        CommonMethods.filterList();
    }

    @Override
    public void newsEdit(int position) {
        Intent intent = new Intent(getContext(), AddNewsActivity.class);
        intent.putExtra("operation", "Edit");
        intent.putExtra("item", MainActivity.lst_news.get(position));
        intent.putExtra("position", String.valueOf(position));
        startActivity(intent);
        adapter.notifyDataSetChanged();
        CommonMethods.filterList();
    }

    @Override
    public void newsDelete(int position) {
        builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure to delete the item?")
                .setTitle("Confirmation")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.lst_news.remove(position);
                        CommonMethods.showToast(getContext(), "Deleted Successfully");
                        adapter.notifyItemRemoved(position);
                        CommonMethods.filterList();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }
}