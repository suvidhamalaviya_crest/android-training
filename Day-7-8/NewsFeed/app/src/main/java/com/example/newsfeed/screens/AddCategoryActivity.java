package com.example.newsfeed.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.utils.CommonMethods;

public class AddCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    public EditText edt_cat_name;
    public Button btn_add_cat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_category);

        initComponent();
    }

    private void initComponent() {
        edt_cat_name = findViewById(R.id.edt_cat_name);
        btn_add_cat = findViewById(R.id.btn_add_cat);

        btn_add_cat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Drawable error_icon = getResources().getDrawable(R.drawable.ic_error);
        error_icon.setBounds(0, 0, error_icon.getIntrinsicWidth(), error_icon.getIntrinsicHeight());

        if (view == btn_add_cat) {
            String cat_name = edt_cat_name.getText().toString().trim();

            if (cat_name.isEmpty()) {
                edt_cat_name.setError("Required", error_icon);
                return;
            }

            MainActivity.lst_category.add(0, cat_name);
            CommonMethods.showToast(this, getString(R.string.add_success));
            finish();
        }
    }
}