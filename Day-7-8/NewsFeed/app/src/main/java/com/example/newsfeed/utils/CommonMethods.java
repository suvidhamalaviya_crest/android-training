package com.example.newsfeed.utils;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.models.NewsFeed;

import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CommonMethods
{

    public static void showToast(Context context,String msg){
        TextView custom_toast_message;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View layout = inflater.inflate(R.layout.custom_toast,null);
        custom_toast_message = layout.findViewById(R.id.custom_toast_message);
        custom_toast_message.setText(msg);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();

    }

    public static void filterList() {
        MainActivity.lst_edu.clear();
        MainActivity.lst_covid.clear();

        /*Predicate<NewsFeed> by_edu = newss -> newss.getCategory().equals("Education");
        Predicate<NewsFeed> by_covid = newss -> newss.getCategory().equals("Covid");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            MainActivity.lst_edu = MainActivity.lst_news.stream().filter(by_edu)
                    .collect(Collectors.toList());
            MainActivity.lst_covid = MainActivity.lst_news.stream().filter(by_covid)
                    .collect(Collectors.toList());
        }*/

        for (NewsFeed news : MainActivity.lst_news) {
            if (news.getCategory().equalsIgnoreCase("Education")) {
                MainActivity.lst_edu.add(news);
            }
            if (news.getCategory().equalsIgnoreCase("Covid")) {
                MainActivity.lst_covid.add(news);
            }
        }

    }
}
