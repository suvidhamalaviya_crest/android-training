package com.example.newsfeed.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;

public class SplashActivity extends AppCompatActivity {

    View first_line,second_line,third_line,fourth_line,fifth_line;
    TextView tv_title;
    AppCompatImageView iv_logo;

    Animation top_animation,middle_animation,bottom_animation;

    private final int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        initComponent();

        setAnimation();

        goNext();
    }

    private void initComponent() {
        first_line = findViewById(R.id.first_line);
        second_line = findViewById(R.id.second_line);
        third_line = findViewById(R.id.third_line);
        fourth_line = findViewById(R.id.fourth_line);
        fifth_line = findViewById(R.id.fifth_line);
        tv_title = findViewById(R.id.tv_title);
        iv_logo = findViewById(R.id.iv_logo);

        top_animation = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        middle_animation = AnimationUtils.loadAnimation(this,R.anim.middle_animation);
        bottom_animation = AnimationUtils.loadAnimation(this,R.anim.botton_animation);
    }


    private void setAnimation() {
        first_line.setAnimation(middle_animation);
        second_line.setAnimation(middle_animation);
        third_line.setAnimation(middle_animation);
        fourth_line.setAnimation(middle_animation);
        fifth_line.setAnimation(middle_animation);
        tv_title.setAnimation(bottom_animation);
        iv_logo.setAnimation(top_animation);
    }

    private void goNext(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
}