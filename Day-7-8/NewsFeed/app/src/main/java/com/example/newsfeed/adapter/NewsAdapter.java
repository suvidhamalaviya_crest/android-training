package com.example.newsfeed.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsfeed.MainActivity;
import com.example.newsfeed.R;
import com.example.newsfeed.models.NewsFeed;
import com.example.newsfeed.screens.AddNewsActivity;
import com.example.newsfeed.utils.CommonMethods;
import com.example.newsfeed.utils.NewsHandlingListener;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder>{

    List<NewsFeed> lst_newsFeed;
    Context context;
    NewsHandlingListener newsHandlingListener;
    boolean is_main;

    public NewsAdapter(Context context, List<NewsFeed> lst_newsFeed,NewsHandlingListener newsHandlingListener,boolean is_main) {
        this.lst_newsFeed = lst_newsFeed;
        this.context = context;
        this.newsHandlingListener = newsHandlingListener;
        this.is_main = is_main;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.news_tile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(!is_main){
            holder.btn_edit.setVisibility(View.GONE);
            holder.btn_delete.setVisibility(View.GONE);
        }
        holder.tv_title.setText(lst_newsFeed.get(position).getTitle());
        holder.tv_desc.setText(lst_newsFeed.get(position).getDescription());
        holder.tv_date.setText(lst_newsFeed.get(position).getDate());
        holder.tv_time.setText(lst_newsFeed.get(position).getTime());
        holder.tv_category.setText(lst_newsFeed.get(position).getCategory());

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newsHandlingListener.newsEdit(position);
            }
        });

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newsHandlingListener.newsDelete(position);
            }
        });

        holder.btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText("Name : " + lst_newsFeed.get(position).getTitle()+"\nDescription : " + lst_newsFeed.get(position).getDescription());
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("news","Name : " + lst_newsFeed.get(position).getTitle()+"\nDescription : " + lst_newsFeed.get(position).getDescription());
                    clipboard.setPrimaryClip(clip);
                }
                CommonMethods.showToast(context, "Copied Successfully");
            }
        });

        holder.btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "News Feed");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "==========================================" +
                        "\nName : " + lst_newsFeed.get(position).getTitle()+
                        "\nCategory : " + lst_newsFeed.get(position).getCategory()+
                        "\nDescription : " + lst_newsFeed.get(position).getDescription()+
                        "\nDate : " + lst_newsFeed.get(position).getDate()+
                        "\nTime : " + lst_newsFeed.get(position).getTime()+
                        "\n==========================================");
                context.startActivity(Intent.createChooser(shareIntent, "Share via"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return lst_newsFeed.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_desc, tv_date, tv_time, tv_category;
        CardView cv_news;
        ImageView btn_edit,btn_delete,btn_copy,btn_share;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_category = itemView.findViewById(R.id.tv_category);
            cv_news = itemView.findViewById(R.id.cv_news);
            btn_edit = itemView.findViewById(R.id.btn_edit);
            btn_delete = itemView.findViewById(R.id.btn_delete);
            btn_copy = itemView.findViewById(R.id.btn_copy);
            btn_share = itemView.findViewById(R.id.btn_share);

            itemView.setOnCreateContextMenuListener((View.OnCreateContextMenuListener) context);
        }

    }
}
